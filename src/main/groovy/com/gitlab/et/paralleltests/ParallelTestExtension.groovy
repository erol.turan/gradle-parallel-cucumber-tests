package com.gitlab.et.paralleltests


class ParallelTestExtension {

    boolean printConfiguration = false
    int parallelExecutions = 1
    String cucumberCliClass = 'cucumber.api.cli.Main'
    String properties = ''
    String glue = ''
    String featureDir = ''
    String tags = ''
    String environmentVariablePrefix = ''
    boolean strict = false
    boolean printTestOutput = true
    boolean parallelScenarios = false
    boolean shortenPathForLogging = false
    List<String> cucumberPlugins = []

}
