package com.gitlab.et.paralleltests.helper

import com.gitlab.et.paralleltests.ParallelTestExtension
import com.gitlab.et.paralleltests.helper.model.Feature
import com.gitlab.et.paralleltests.helper.model.Scenario
import com.gitlab.et.paralleltests.helper.model.Test
import com.gitlab.et.paralleltests.helper.report.CucumberPluginResolver

class TestGenerator {

    ParallelTestExtension extension
    CucumberPluginResolver cucumberPluginResolver = new CucumberPluginResolver()

    TestGenerator(ParallelTestExtension extension, CucumberPluginResolver cucumberPluginResolver) {
        this.extension = extension
        this.cucumberPluginResolver = cucumberPluginResolver
    }


    List<Test> generateTestsForFeature(Feature feature) {
        List<Test> tests = []

        if (feature.scenarios && extension.parallelScenarios) {
            feature.scenarios.each { scenario ->
                tests.add(generateTestWithScenario(feature, scenario))
            }
        } else {
            tests.add(generateBaseTest(feature))
        }

        return tests
    }

    List<Test> generateTestsForFeatures(List<Feature> features) {
        List<Test> tests = []
        features.each { tests.addAll(generateTestsForFeature(it)) }

        if (extension.shortenPathForLogging) {
            reducingPathInTestNamesByHighestPossibleEquality(tests)
        }

        return tests

    }


    private Test generateTestWithScenario(Feature feature, Scenario scenario) {
        Test test = generateBaseTest(feature)
        test.command.addAll(
                [
                        "--name",
                        scenario.name
                ]
        )
        test.name = test.name + ":" + scenario.name[1..-2]
        return test

    }


    private Test generateBaseTest(Feature feature) {
        Test test = new Test()
        test.name = feature.path
        test.command.add("java")
        test.command.addAll(getSplittedExtensionProperties())
        test.command.addAll(getEnvironmentVariables())
        test.command.addAll(
                [
                        extension.cucumberCliClass,
                        "--glue",
                        extension.glue,
                        extension.tags ? "--tags" : "",
                        extension.tags ? extension.tags : "",
                        extension.strict ? "--strict" : "",
                ])

        extension.cucumberPlugins.each {
            test.command.addAll(["-p", cucumberPluginResolver.pluginWithRandomizedFileName(it)])
        }
        test.command.add(feature.path)
        test.command.removeAll(["", null])
        return test
    }

    private List<String> getEnvironmentVariables() {
        def vars = EnvironmentVariableRetriever.getEnvVarsWithoutPrefix(extension.environmentVariablePrefix)
        List<String> varsAsStringList = []
        vars.each { varsAsStringList.add "-D$it.key=$it.value" }
        return varsAsStringList
    }

    private List<String> getSplittedExtensionProperties() {
        if (extension.properties.isEmpty()) {
            return []
        }
        List<String> tmpProps = []
        tmpProps = extension.properties.split("-D")
        tmpProps.removeAll([""])
        List<String> props = []
        tmpProps.each { props.add "-D$it" }
        return props

    }


    private void reducingPathInTestNamesByHighestPossibleEquality(List<Test> tests) {
        String equalSubPath = ""
        if (tests.first()) {
            String baseTestName = tests.first().name

            for (String c : baseTestName) {
                if (tests.name.every { it.startsWith(equalSubPath + c) }) {
                    equalSubPath += c
                } else {
                    break
                }
            }
        }


        int i = equalSubPath.lastIndexOf(equalSubPath.split(/[\/\\]/).last())
        equalSubPath = equalSubPath.substring(0, i)

        tests.each { it.name = it.name.replace(equalSubPath, "") }
    }


}
