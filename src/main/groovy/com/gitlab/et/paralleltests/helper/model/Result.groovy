package com.gitlab.et.paralleltests.helper.model

enum Result {
    PASSED,
    FAILED,
    NOT_RUN
}