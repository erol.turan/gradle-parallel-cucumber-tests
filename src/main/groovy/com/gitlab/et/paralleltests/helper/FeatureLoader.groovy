package com.gitlab.et.paralleltests.helper

import com.gitlab.et.paralleltests.ParallelTestExtension
import com.gitlab.et.paralleltests.helper.model.Feature
import com.gitlab.et.paralleltests.helper.model.Scenario

import java.nio.file.Path
import java.nio.file.Paths

class FeatureLoader {

    ParallelTestExtension extension
    String baseDir

    FeatureLoader(ParallelTestExtension extension, String baseDir) {
        this.extension = extension
        this.baseDir = baseDir
    }


    List<Feature> locateFeatures() {
        List<Feature> features = []

        Path absoluteFeaturePath = Paths.get(baseDir, extension.featureDir)
        List<String> featureFilesAbsolutePath = new FileNameByRegexFinder()
                .getFileNames(absoluteFeaturePath.toString(), /.*\.feature$/)

        featureFilesAbsolutePath.each {
            features.add(
                    new Feature(path: PathShortener.getRelativePathOf(it))
            )
        }

        features.each {
            loadScenariosIntoFeature(it)
        }

        return features
    }

    private static loadScenariosIntoFeature(Feature feature) {
        List<String> lines = new File(feature.path).readLines()

        for (line in lines) {
            line = line.trim()
            if (line.startsWith("Scenario:") || line.startsWith("Scenario Outline:")) {
                String scenarioName = line
                scenarioName = scenarioName.replace("Scenario:", "")
                scenarioName = scenarioName.replace("Scenario Outline:", "")
                scenarioName = scenarioName.trim()
                feature.scenarios.add(new Scenario(name: "^" + scenarioName + "\$"))
            }

        }

    }


}
