package com.gitlab.et.paralleltests.helper

import java.nio.file.Paths

class PathShortener {

    static String getRelativePathOf(String pathAsString) {
        return Paths.get(System.getProperty("user.dir")).relativize(new File(pathAsString).toPath()).toString()
    }
}
