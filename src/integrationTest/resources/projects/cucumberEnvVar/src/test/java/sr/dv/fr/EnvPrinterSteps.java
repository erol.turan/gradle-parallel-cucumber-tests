package sr.dv.fr;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.assertj.core.api.Assertions.assertThat;


public class EnvPrinterSteps {

    private EnvPrinter printer = new EnvPrinter();
    String envVar = "";

    @Given("var \"(.*)\" is going to be checked")
    public void var_is_going_to_be_checked(String envVar) {
        this.envVar=envVar;
    }

    @Then("var has the value \"(.*)\"")
    public void var_has_the_value(String value) {
        String var = printer.getEnvVar(envVar);
        assertThat(var).isEqualTo(value);
    }


}
